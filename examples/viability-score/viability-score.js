require([
	'lib/dnbdirect-2.0.0',
//	'lib/knockout-2.2.1',
	'text!components/viability-score.html'
	], function(dnb, ko, txt) {
		console.log(ko)
    function ViewModel(duns)
    {
        var self = this;
        self.duns = ko.observable(duns);
        self.assessment = DNB.Api.ko('V3.0/organizations/{duns}/products/VIAB_RAT?CountryISOAlpha2Code=US&ApplicationTransactionId=automation', self);// 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment');
    }

    function DriverViewModel(assessmentVm)
    {
        var duns = '';
        self.duns = ko.observable(duns);
        ko.computed(function(){
            assessmentVm.duns(self.duns());
        });
    }

    var duns_number = 804735132;

        var assessmentVm = new ViewModel(duns_number);
        ko.applyBindings(assessmentVm, $('#x-dnb-viab-rat')[0]);



        var vm = new DriverViewModel(assessmentVm);
        ko.applyBindings(vm, $('.x-dnb-search')[0]);

		console.log(dnb);

//	console.log(DNB);
    //This function is called when scripts/helper/util.js is loaded.
    //If util.js calls define(), then this function is not fired until
    //util's dependencies have loaded, and the util argument will hold
    //the module value for "helper/util".
});