//
window.alertx = function(txt, is_html)
{
	if ($('#win_float').length==0)
	{
		var html = '\
			<div id="screen_mask" class="mask" style="height:10000px;display:block;"></div>\
			<div id="win_float" class="pop_container">\
				<div class="popup">\
					<div class="pop_header">Demo Information</div>\
					<div class="pop_content"></div>\
					<div class="pop_description">\
						<div id="hide_win2" class="action_btn">Close</div>\
						<div class="clear"></div>\
					</div>\
					<div id="hide_win" class="close_btn"> X </div> \
				</div>\
			</div>\
			';
		$('body').append(html);


		$("#hide_win,#hide_win2").click(function(){
				
				   
				   	$("#screen_mask").fadeTo( 200, 0.0, function () {
            				$("#win_float").css('visibility','hidden'); 
				    		$("#screen_mask").css('visibility','hidden');
          			});
						
				 
				   
				   	$("#win_float").fadeTo( 200, 0.0, function () {
            				$("#win_float").css('visibility','hidden'); 
				    		$("#screen_mask").css('visibility','hidden');
          			});
				   
		
			  });
	}

	if (is_html)
		$('#win_float .pop_content').html(txt);
	else
		$('#win_float .pop_content').text(txt);

	$("#win_float").css('visibility','visible');
	$("#screen_mask").css('visibility','visible');
	$("#screen_mask").fadeTo(400,'0.5');
	$("#win_float").fadeTo(400,'1.0');

}

// The D&B API JS must be loaded here, otherwise fail out

'use strict';

/**
 *  @namespace The DNB Javascript Library.
 */
var DNB = DNB || {};

if (DNB.Api === undefined) throw 'Error:  DNB API Library missing';

/**
 *  @namespace The DNB Demo Library
 */
DNB.Demo = DNB.Demo || {};


DNB.Demo.signin = function()
{
	var html = '<form id="signin">\
	<label>API-KEY:</label><br/>\
	<input type="text" name="API-KEY" /><br /><br />\
	<label>Username:</label><br/>\
	<input type="text" name="username" /><br /><br />\
	<label>Password:</label><br/>\
	<input type="password" name="password" /><br /><br />\
	<input type="submit" value="OK" />\
	</form>';
//	alert(html,1);
// 	$('#win_float .pop_header').text('D&B Direct API Credentials');
	$('#signinModal').modal();

	$('#signinModal form').on('submit', function(ev){
		ev.preventDefault();

		$.ajax({
			url: DNB.Api.API_BASE + '/_demo/auth',
			type: 'POST',
			data: $('#signin').serialize()
		})
			.done(function(resp){
				alert('Credentials have been saved. You will now have full-access to D&B Direct.');
				$('#signinModal').modal('hide');
			})
			.fail(function(a){
				alert("Sorry, there was an error updating credentials.");
			})
	});
}

DNB.Demo.signout = function()
{
	$.ajax({
		url: DNB.Api.API_BASE + '/_demo/auth',
		type: 'DELETE' 
	});
}